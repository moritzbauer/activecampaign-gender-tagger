ActiveCampaign doesn't have the possibility to tag a lead with it's gender. So I programmed it myself. 

To make it run:

* Edit the setup section in gender_tagger.py
  * Put your API URL (you'll find that under: "Settings > Developer" in your account)
  * Put your API Key
  * Put the list id whose contacts you want to tag (to find the list id, go to Lists, then click on the list and in the URL there is a parameter which reveals the id. Example: `https://moritzbauer.activehosted.com/app/contacts?limit=100&listid=16&status=1`

If you want, you can also edit the presets to rename the tags.

Default is:

* Gender:Male
* Gender:Female
* Gender:Unknown

Then just run the script with:

    python gender_tagger.py 

It should pretty much work out of the box, if you have python installed.

Cheers,
Moritz