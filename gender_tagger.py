from lib.activecampaign.client import Client
import lib.gender_guesser.detector as gender
import time
from time import perf_counter

### SETUP ###
###
your_url = ""
your_api_key = ""
your_list_id = ""

### presets
# if script dies for whatever reason, put offset to the number of the tag that was last tagged, then the script starts working from there...
start_at = 0
gender_name_tag_male = "Gender:Male"
gender_name_tag_female = "Gender:Female"
gender_name_tag_unknown = "Gender:Unknown"
####


print("Starting gender tagger...")

#create connection and gender detector object
client = Client(your_url, your_api_key)
d = gender.Detector(case_sensitive=False)

#try to create tags
data_male = {
	"tag":{
		"tag": gender_name_tag_male,
		"tagType": "contact",
		"description": gender_name_tag_male
	}
}
data_female = {
	"tag":{
		"tag": gender_name_tag_female,
		"tagType": "contact",
		"description": gender_name_tag_female
	}
}
data_unknown = {
	"tag":{
		"tag": gender_name_tag_unknown,
		"tagType": "contact",
		"description": gender_name_tag_unknown
	}
}

# create tags

print("Creating Tag: " + gender_name_tag_male)
client.contacts.create_a_tag(data_male)
print("Creating Tag: " + gender_name_tag_female)
client.contacts.create_a_tag(data_female)
print("Creating Tag: " + gender_name_tag_unknown)
client.contacts.create_a_tag(data_unknown)

#get all tag ids
all_tags = client.contacts.list_all_tags(limit="100")
total_tags = all_tags["meta"]["total"]
num_tags = 0


print("Getting Tag Ids...")
while num_tags < int(total_tags):
    try:
        id_tag_male = next(item for item in all_tags["tags"] if item["tag"] == gender_name_tag_male)["id"]
    except:
        pass
    try:
        id_tag_female = next(item for item in all_tags["tags"] if item["tag"] == gender_name_tag_female)["id"]
    except:
        pass
    try:
        id_tag_unknown = next(item for item in all_tags["tags"] if item["tag"] == gender_name_tag_unknown)["id"]
    except:
        pass

    time.sleep(1)
    num_tags += 100
    all_tags = client.contacts.list_all_tags(limit="100", offset=num_tags)


print("Getting contacts...")

response = client.contacts.list_all_contacts(listid=your_list_id,status="1",limit="100",offset=start_at)
total = response["meta"]["total"]
num = 0
cnt = 0

genderdict = {
    "male":id_tag_male,
    "mostly_male":id_tag_male,
    "female":id_tag_female,
    "mostly_female":id_tag_female,
    "unknown":id_tag_unknown,
    "andy":id_tag_unknown
}


print("Number of contacts: " + str(total) + " Offset: " + str(start_at))

while num < int(total):
    t1_start = perf_counter()

    for contact in response["contacts"]:
        cnt += 1
        gender = d.get_gender(contact["firstName"])   #get gender of text in firstName field
        ac_id = contact["id"]

        data = {
	       "contactTag": {
		         "contact": ac_id,
		         "tag": genderdict[gender]
	       }
        }

        response = client.contacts.add_a_tag_to_contact(data)

        if(response):
            print(str(cnt) + ": " + contact["id"] + " " + contact["email"] + " " + contact["firstName"] + " - " + gender)

        #if we are at 5 requests, wait a second API Rate limit!
        if(cnt%5 == 0):
           time.sleep(1)

        #print(data)


    num += 100
    response = client.contacts.list_all_contacts(listid=your_list_id,status="1",limit="100",offset=num)

    t1_stop = perf_counter()
    print("############# Elapsed time in seconds:", t1_stop-t1_start)
